//
//  ViewController.swift
//  sportMotivator
//
//  Created by toto on 30/05/2018.
//  Copyright © 2018 Mathieu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var exerciseField: UITextField!
    @IBOutlet weak var repetitionsField: UITextField!
    @IBOutlet weak var myTableView: UITableView!
    
    var mySportList:List!
    
    
    
    func myTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)->
        UITableViewCell{
            let cell =
                UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell");
            cell.textLabel?.text = mySportList.getList()[indexPath.row];
            return (cell)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        mySportList = List(sportTableView: myTableView)
        
    }
    
    @IBAction func handleAddItem(_ sender: Any) {
        mySportList.addElement(exercice: exerciseField.text!, repetition: repetitionsField.text!)
        myTableView.reloadData();
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

