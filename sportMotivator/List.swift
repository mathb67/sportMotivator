//
//  list.swift
//  sportMotivator
//
//  Created by toto on 30/05/2018.
//  Copyright © 2018 Mathieu. All rights reserved.
//

import Foundation
import UIKit

class List: NSObject {
    
    var exerciseTableView: UITableView
    var exercisesList = [String]()
    
    init(sportTableView  tableView:UITableView) {
        exerciseTableView = tableView;
    }
    
    func addElement(exercice: String, repetition: String) {
        exercisesList.append(exercice + " : " + repetition + " fois.");
    }
    
    func getList() -> [String] {
        return exercisesList;
    }
    
    func countList() -> Int {
        return (exercisesList.count);
    }
}
